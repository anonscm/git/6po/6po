#!/usr/bin/perl
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

use strict;
use warnings;
use URI::Encode qw(uri_decode);
use URI::Escape;
use POSIX;
use YAML::Tiny;
use File::Copy;
use Encode qw( decode_utf8 encode_utf8 );

$SIG{CHLD} = 'IGNORE';

my $yaml = YAML::Tiny->read('/etc/6po/6po.yml') or die "Impossible d'ouvrir le fichier /etc/6po/6po.yml" ;
my $SISPODAV = $yaml->[0]->{sispodav};
my $SISPO = $yaml->[0]->{sispo};
my $SERVERS=$yaml->[0]->{servers};
my %servers_ip;
my $fifpo="/tmp/fifpo";

foreach my $server (keys(%{$SERVERS})) {
  my $SERVER=$SERVERS->{$server};
	if (! defined $SERVER->{"site"}) { $servers_ip{"_default_"}=$SERVER->{"ip"}; }
	else { $servers_ip{$SERVER->{"site"}}=$SERVER->{"ip"}; }
}

while (<STDIN>) {
	if ($_=~/^[0-9\.]* - [a-zA-Z0-9_\.\-]* [^ ]* +[^ ]* \"PUT [^ ]* HTTP\/1.1\" 201/) {
		unless (fork) {
			my @tab_entree=split(' ', $_);
			my $ip=$tab_entree[0];
			my $login=$tab_entree[2];
			my $date=$tab_entree[3];
			my $gmt=$tab_entree[4];
			my $chemin=decode_utf8(uri_decode($tab_entree[6]));
			sleep(5);
			my $tmpdir=$SISPODAV->{tmpdir}.$login."/";
			mkdir $tmpdir;
			move($SISPODAV->{dir}.$chemin,$tmpdir);
			my @tab_chemin=split('/',$chemin);
			my $noir_et_blanc=0;
			my $index=-2;
			my $duplex="recto";
			my $couleur="couleur";
			my $server_ip;
			if (defined $servers_ip{"_default_"}) { $server_ip=$servers_ip{"_default_"}; }
			else { $server_ip=$servers_ip{$tab_chemin[1]}; }
			while ($tab_chemin[$index] eq "recto" || $tab_chemin[$index] eq "recto-verso" || $tab_chemin[$index] eq "recto-verso-bord-court" || $tab_chemin[$index] eq "couleur" || $tab_chemin[$index] eq "noir_et_blanc") {	
				if ($tab_chemin[$index] eq "recto-verso" || $tab_chemin[$index] eq "recto-verso-bord-court") { $duplex=$tab_chemin[$index]; }
				elsif ($tab_chemin[$index] eq "noir_et_blanc") { $couleur="noir_et_blanc" }
				$index--;
			}
			my $imprimante=$tab_chemin[$index];
			open(my $fifpo_fh, "> $fifpo") || die "impossible d'écrite dans $fifpo : $! \n";
			#print $fifpo_fh "$ip $login $imprimante $server_ip $couleur $duplex 1 1 - A4 portrait $tmpdir ".uri_escape($tab_chemin[$#tab_chemin])."\n";
			print $fifpo_fh "$ip $login $imprimante $server_ip $couleur $duplex 1 1 - A4 portrait $tmpdir ".uri_escape(encode_utf8($tab_chemin[$#tab_chemin]))."\n";
			exit 0;
		}
	}
}
