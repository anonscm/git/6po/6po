#!/usr/bin/perl
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

use strict;
use warnings;
use POSIX;
use URI::Escape;
use YAML::Tiny;
use File::MimeInfo::Magic qw(mimetype);
use File::Copy;
use File::stat;
use Switch;
use Encode qw(decode_utf8);
use utf8;

sub isImage {
	my ($mimetype)=@_;
	return $mimetype=~/^image\//;
}

sub isText {
	my ($mimetype)=@_;
	return $mimetype=~/^text\//;
}

sub isDoc {
	my ($ext)=@_;
	my @types=("doc","docx","dotm","odt","rtf","wpd");
	for my $type (@types) {
		if ($ext eq ".$type") {
			return 1;
		}
	}
	return 0;
}
		
sub isSpreedSheat {
	my ($ext)=@_;
	my @types=("xls","xlsx","xlsm","ods","tsv","csv");
	for my $type (@types) {
		if ($ext eq ".$type") {
			return 1;
		}
	}
	return 0;
}
		
sub isSlideShow {
	my ($ext)=@_;
	my @types=("ppt","pptx","pptm","pps","ppsx","odp");
	for my $type (@types) {
		if ($ext eq ".$type") {
			return 1;
		}
	}
	return 0;
}
	
sub isDraw {
	my ($ext)=@_;
	my @types=("vsd","odg");
	for my $type (@types) {
		if ($ext eq ".$type") {
			return 1;
		}
	}
	return 0;
}
	
sub isXMLPaper {
	my ($ext)=@_;
	my @types=("xps","opxs");
	for my $type (@types) {
		if ($ext eq ".$type") {
			return 1;
		}
	}
	return 0;
}

sub isHTML {
	my ($mimetype)=@_;
	return ($mimetype eq "text/html");
}

sub fromRange {
	my ($range,$max)=@_;	
	my $etat=0;
	my $n1=0;
	my $n2=0;
	my $nb_pages=0;
	my $ignore;
	my $valid_range="";
	for (my $i=0;$i<length($$range);$i++) {
		switch ($etat) {
			case 0 {
				$n1=ord(substr($$range, $i, 1))-48;
				$ignore=0;
				$etat=1;
			}	
			case 1 {
				if (substr($$range, $i, 1)=~m/^[0-9]/) {
					$n1=$n1*10+(ord(substr($$range, $i, 1))-48);
				}
				elsif (substr($$range, $i, 1) eq "-") {
					if ($n1<=$max) {
						$valid_range.=sprintf("%d-",$n1);	
					} 
					else {
						$ignore=1;
					}
					$etat=2;
				}
				# le caractère est ","
				else {
					if ($n1<=$max) {
						$valid_range.=sprintf("%d ",$n1);
						$nb_pages++;	
					} 
					$etat=0;
				}
			}
			case 2 {
					$n2=ord(substr($$range, $i, 1))-48;
					$etat=3;
			}
			case 3 {
				if (substr($$range, $i, 1)=~m/^[0-9]/) {
					$n2=$n2*10+(ord(substr($$range, $i, 1))-48);
				}
				else { 
					# le caratère est ","
					if (! $ignore) {
						if ($n2<=$max) {
							$valid_range.=sprintf("%d ",$n2);
							$nb_pages+=($n2-$n1+1);
						}
						else {
							$valid_range.=sprintf("%d ",$max);
							$nb_pages+=($max-$n1+1);
						}
					}	
					$etat=0;
				}
			} 
		}
	}
	if ($etat==1) {
		if ($n1<=$max) { 
			$valid_range.=sprintf("%d",$n1);
			$nb_pages++; }
	} else {
		if ($n2>=$max) { 
			$nb_pages+=$max-$n1+1; 
			$valid_range.=sprintf("%d",$max);
		}
		else { 
			$nb_pages+=$n2-$n1+1; 
			$valid_range.=sprintf("%d",$n2);
		}
	}
	$$range=$valid_range;
	return  $nb_pages;
}

sub PDFCount{
	my ($chemin_pdf,$duplex,$ppp,$copies,$range)=@_;
	my $nb_pages;
	open(PDFINFO,"pdfinfo \"$chemin_pdf\" | grep ^Pages|"); 
	$nb_pages=<PDFINFO>;
	$nb_pages=~s/Pages:\s+([0-9]*)/$1/;
	chomp($nb_pages);
	if ($$range ne "-" ) {
		$nb_pages=fromRange($range,$nb_pages);
	}
	$nb_pages=int($nb_pages/$ppp)+($nb_pages%$ppp?1:0);
	if ($duplex eq "recto-verso" || $duplex eq "recto-verso-bord-court") {
		$nb_pages=int($nb_pages/2)+($nb_pages%2?1:0);
	}
	return $nb_pages*$copies;
}

sub WriteLog{
	my ($chemin_log,$message)=@_;
	open(LOGFILE,">>:encoding(UTF-8)",$chemin_log);
	select((select(LOGFILE), $|=1)[0]);
	print LOGFILE "$message\n";
	close(LOGFILE);
}

my $yaml = YAML::Tiny->read("/etc/6po/6po.yml") or die "Impossible d'ouvrir le fichier /etc/6po/6po.yml" ;
my $SISPO = $yaml->[0]->{sispo};
my $SERVERS=$yaml->[0]->{servers};
my $SISPOMAIL=$yaml->[0]->{sispomail};
my $KERBEROS=$yaml->[0]->{kerberos};

my %servers_ip;
foreach my $server (keys(%{$SERVERS})) {
  my $SERVER=$SERVERS->{$server};
	if ($SERVER->{"type"} eq "windows") { $servers_ip{$SERVER->{"ip"}}="127.0.0.1"; }
	else { $servers_ip{$SERVER->{"ip"}}=$SERVER->{"ip"}; }
}

$SIG{CHLD} = 'IGNORE';
local $SIG{'PIPE'} = 'IGNORE';

my $fifpo="/tmp/fifpo";
unless ( -p $fifpo) {
	if (-e $fifpo) {
		die "$0: ne peut écraser le fichier $fifpo.\n";
	} else {
		umask 000;
		mkfifo($fifpo, 0666);
	}
}

open(my $fifpo_fh, "+< $fifpo") || die "impossible de lire dans $fifpo : $! \n";

my $SISPOLOG=$SISPO->{logdir}."jobs.log";

while (<$fifpo_fh>) {
	unless (fork) {
		my $date = strftime "[%d/%m/%Y:%H:%M:%S]", localtime;
		my ($ip,$login,$imprimante,$server_ip,$couleur,$duplex,$copies,$ppp,$range,$format,$orientation,$tmpdir,$encoded_fichier)=split(' ', $_);
		my $fichier=decode_utf8(uri_unescape($encoded_fichier));
		my $chemin=$tmpdir.$fichier;
		my $chemin_sans_double_quotes=$chemin;
		$chemin_sans_double_quotes=~ s/"//g;
		if ($chemin ne $chemin_sans_double_quotes) {
			rename $chemin,$chemin_sans_double_quotes;
			$chemin=$chemin_sans_double_quotes;
		}
		my $mimetype=mimetype($chemin);		
		my ($ext)= lc($encoded_fichier) =~ /(\.[^.]+)$/;
		my $chemin_pdf=substr($chemin,0,-(length($ext))).".pdf";
		WriteLog($SISPOLOG,"$ip - $date $login a demandé l'impression de $fichier de type $mimetype et extension $ext sur $imprimante du serveur $server_ip ".sprintf("[couleur=%s,duplex=%s,copies=%s,ppp=%s,range=%s,format=%s,orientation=%s]",$couleur,$duplex,$copies,$ppp,$range,$format,$orientation));
		if ($ext eq ".pdf") {
			if (! ($chemin_pdf eq $chemin) ) {
				copy($chemin,$chemin_pdf);
			}
		} else {
			if ($ext eq ".url") {
				open (my $urlfile, '<', "$chemin"); 
				my $url = <$urlfile>; 
				close $urlfile;
				system("wkhtmltopdf -q \"$url\" \"$chemin_pdf\"");
			}
			# Il faut tester le type html avant le type text pour qu'il ne soit pas traité par office
			elsif (isHTML($mimetype)) {
				system("wkhtmltopdf --encoding utf-8 -q \"$chemin\" \"$chemin_pdf\"");
			}
			elsif (isImage($mimetype)) {
				system("convert \"$chemin\" \"$chemin_pdf\"");
			}
			elsif (isDoc($ext) || isText($mimetype)) {
				WriteLog($SISPOLOG,"/usr/bin/lowriter --headless --convert-to pdf:writer_pdf_Export \"$chemin\" --outdir $tmpdir");
				system("lowriter --headless --convert-to pdf:writer_pdf_Export \"$chemin\" --outdir $tmpdir");
			}
			elsif (isSpreedSheat($ext)) {
				system("localc --headless --convert-to pdf:writer_pdf_Export \"$chemin\" --outdir $tmpdir");
			}
			elsif (isSlideShow($ext)) {
				system("loimpress --headless --convert-to pdf:writer_pdf_Export \"$chemin\" --outdir $tmpdir");
			}
			elsif (isDraw($ext)) {
				system("lodraw --headless --convert-to pdf:writer_pdf_Export \"$chemin\" --outdir $tmpdir");
			}
			elsif (isXMLPaper($ext)) {
				system("xpstopdf \"$chemin\" \"$chemin_pdf\"");
			}
		}
		if (-f $chemin_pdf) {
			if ($SISPO->{accounting} || $range ne "-") {
				my $nb_pages=PDFCount($chemin_pdf,$duplex,$ppp,$copies,\$range);
				if ($SISPO->{accounting} && $range) {
					WriteLog($SISPO->{logdir}."accounting_$server_ip.log","$date 6po_logaccounting: $imprimante $login $nb_pages $couleur");
				}
			}
			if ($range) {
				my $ip_server=$servers_ip{$server_ip}; 
				if ($ip_server eq "127.0.0.1") { $imprimante=$server_ip."_".$imprimante; }
				my $gs="";
				my $cmd;
				my $lpr="lpr";
				my $options=" -o fit-to-page -o media=$format";
				if ($couleur eq "noir_et_blanc") { 
					$gs="| gs -sstdout=%stderr -sOutputFile=%stdout -q -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -sColorConversionStrategy=Gray -sColorConversionStrategyForImages=Gray -sProcessColorModel=DeviceGray -dCompatibilityLevel=1.4 -dNOPAUSE -dOverrideICC -dBATCH -";
				} 
				elsif (($chemin eq $chemin_pdf) && $SISPO->{repair_pdf}) {
					$gs="| gs -sstdout=%stderr -sOutputFile=%stdout -q -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dOverrideICC -dBATCH - ";
				} 
				if ($duplex eq "recto") { $options.=" -o sides=one-sided" }
				elsif ($duplex eq "recto-verso") { $options.=" -o sides=two-sided-long-edge" }
				else { $options.=" -o sides=two-sided-short-edge" }
				$options.=" -#$copies -o number-up=$ppp";
				if ($orientation eq "paysage") { $options.=" -o landscape"; }
				my $suexec_user="root";
				if ($SISPO->{suexec}) {
					if (defined $SISPO->{suexec_user}) { $suexec_user=$SISPO->{suexec_user}; }
					else { $suexec_user=$login; }
				}
				my $titre=$fichier;
				$titre=~ s/"/\\"/g;
				if ($range eq "-") { $cmd="cat \"$chemin_pdf\""; }
				else { $cmd="pdftk \"$chemin_pdf\" cat $range output -"; } 
				if ($KERBEROS->{active}) {
					$lpr="KRB5CCNAME=".$KERBEROS->{ticket}." lpr";
					$titre=$login." ".$titre;
				}
				WriteLog($SISPOLOG,"$ip - $date $cmd $gs | $lpr -U$suexec_user -P$imprimante -H$ip_server -T\"$titre\" $options");
				system("$cmd $gs | $lpr -U$suexec_user -P$imprimante -H$ip_server -T\"$titre\" $options");
			} else {
				WriteLog($SISPOLOG,"$ip - $date la plage de pages demandée est au-delà de la dernière page du document $fichier !\n"); 
			}
		}
		else {
			WriteLog($SISPOLOG,"$ip - $date le document $fichier n'a pas pu être converti en PDF !\n"); 
		}
		if ($tmpdir=~ /^$SISPOMAIL->{tmpdir}/) {
			if (!($chemin eq $chemin_pdf)) { unlink($chemin_pdf); }
		} else {
			if (-f $chemin) { unlink($chemin); }	
			if (-f $chemin_pdf) { unlink($chemin_pdf); }	
		}
		exit 0;
	}
}

close($fifpo_fh);
