#!/usr/bin/perl
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#
use strict;
use warnings;
use Email::MIME::Attachment::Stripper;
use IO::File;
use Net::LDAP;
use YAML::Tiny;
use File::Path qw(make_path remove_tree);

my $yaml = YAML::Tiny->read("/etc/6po/6po.yml") or die "Impossible d'ouvrir le fichier /etc/6po/6po.yml" ;
my $SISPOMAIL = $yaml->[0]->{sispomail};
my $LDAP = $yaml->[0]->{ldap};
my $mail="";

while (my $line = <>) {
   $mail.=$line;
}

my $stripper = Email::MIME::Attachment::Stripper->new($mail);

my $msg = $stripper->message;
my @attachments = $stripper->attachments;
my $fh;

my %header=@{$msg->{'header'}->{'headers'}};
my $from=$header{'From'}[0]."\n";

if ($from =~/<([^\@]*)\@([^\>]*)>/) {

	if ( ($SISPOMAIL->{check_domain} && ($SISPOMAIL->{domain} eq $2)) || !($SISPOMAIL->{check_domain})) {

		my $email=$1."@".$2;

		my $lh = Net::LDAP->new($LDAP->{uri},timeout=>5) or die ("LDAP: impossible d'etablir la connexion avec ".$LDAP->{ip});
		my $mesg;
		if ($LDAP->{anonymous}) {
			$mesg = $lh->bind() or die("LDAP: bind anonymous impossible");
		}
		else {
			$mesg = $lh->bind($LDAP->{bind_user}, password => $LDAP->{bind_password}) or die("LDAP: bind impossible");
		}

		my $attribute;
		if ($LDAP->{type} eq "windows") { $attribute="samaccountame"; }
		else { $attribute="uid";  }
		my @attributs=($attribute);

		$mesg = $lh->search(base=>"$LDAP->{user_base_dn}", scope=>"sub", filter => "$LDAP->{mail}=$email", attrs=> [ @attributs ] );
		$mesg->code && die("ldap search : ".$mesg->error);

		foreach my $entry ($mesg->entries) {
			my $tmpdir=$SISPOMAIL->{tmpdir}.$entry->get_value($attribute);
			remove_tree($tmpdir);
			umask(0);
			make_path($tmpdir,{mode => 0755});
			my $n=0;
			if ($mesg->count()!=0) {
				foreach my $attachement (@attachments) {
					if ( ! ($attachement->{filename} eq "")) {
						my $filename=$attachement->{filename};
						my $f = new IO::File $tmpdir ."/". $filename, "w" or die "Impossible de créer le fichier";
						print $f $attachement->{payload};
					}
				}
				local $SIG{'PIPE'} = 'IGNORE';
				open($fh, '|-','/usr/local/bin/pdfconvertme.pl',$tmpdir);
				print {$fh} $mail;
				close($fh);
			}
			$mesg = $lh->unbind;
		}
	}
}

