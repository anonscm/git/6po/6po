#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

function encode{
 param(
  [int64]$k0,
  [int64]$k1,
  [int64]$k2,
  [int64]$k3,
  $chaine
 )
 
 [int64]$delta=2654435769
 [int64]$v1=0
 [int64]$v2=0
 $result=""
 $bytes = [System.Text.Encoding]::UTF8.GetBytes($chaine)
 $r=8-($bytes.length%8)
 for ($i = 0; $i -lt $r; $i++) { $bytes += 0 }
 for ($i = 0; $i -le ($bytes.length-8) ; $i+=8) { 
    $v0=$bytes[$i+3] + ( $bytes[$i+2]*256 )+ ($bytes[$i+1]*65536 ) + ($bytes[$i]*16777216)
    $v1=$bytes[$i+7] + ( $bytes[$i+6]*256 )+ ($bytes[$i+5]*65536 ) + ($bytes[$i+4]*16777216) 
    $sum = 0;
    $n = 0;
    while ($n -lt 32) {
        $sum += $delta 
        $v0 += (($v1*16) +$k0) -bxor ($v1+$sum) -bxor (( ($v1 -band 4294967264) /32)+$k1)   
        $v0 = $v0 -band 4294967295 
        $v1 += (($v0*16) +$k2) -bxor ($v0+$sum) -bxor (( ($v0 -band 4294967264) /32)+$k3)  
        $v1 = $v1 -band 4294967295      
        $n++             
    }
    $result+="{0:X8}" -f $v0
    $result+="{0:X8}" -f $v1
 }
 return $result
}

function GetLocation {
param (
  $Path
)
 (Get-ItemProperty -Path $Path -name location).location
}

function GetDriverName {
param (
  $Path
)
 (Get-ItemProperty -Path $Path -name "Printer Driver")."Printer Driver"
}

function GetDuplex {
param (
  $Path
)
 $Path+="/DsDriver"
 (Get-ItemProperty -Path $Path -name "PrintDuplexSupported")."PrintDuplexSupported"
}

function GetColor {
param (
  $Path
)
 $Path+="/DsDriver"
 (Get-ItemProperty -Path $Path -name "PrintColor")."PrintColor"
}

function GetA3 {
param (
  $Path
)
 $Path+="/DsDriver"
 (Get-ItemProperty -Path $Path -name "PrintMediaSupported")."PrintMediaSupported" -contains "A3"
}

 
$CONF=Import-Csv "C:\6PO\wcollector.csv"
$k0=$CONF.k0
$k1=$CONF.k1
$k2=$CONF.k2
$k3=$CONF.k3

$port=$CONF.port
$domain=$CONF.domain

$endpoint = new-object System.Net.IPEndPoint ([system.net.ipaddress]::any, $port)
$listener = new-object System.Net.Sockets.TcpListener $endpoint
$listener.start()

while (1) {
 $client = $listener.AcceptTcpClient() 
 $stream = $client.GetStream();
 $writer = New-Object System.IO.StreamWriter $stream
 $writer.WriteLine($(encode $k0 $k1 $k2 $k3 "START"))
 $writer.Flush()
 Get-WmiObject -Class Win32_LogicalShareSecuritySetting | ForEach-Object { 
  $path='hklm:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Print\Printers\'+$($_.name);
  if ( Test-Path $path) {
   $ListAllowedUsers='';
   $ListDeniedUsers='';
   &{$_.GetSecurityDescriptor().descriptor.DACL}| Where-Object {(($_.AccessMask -band 131080) -eq 131080 ) -and (($_.Trustee.SIDString -eq "S-1-1-0") -or ($_.Trustee.Domain -eq $domain)) } |ForEach-Object { if ($_.AceType -eq 0) { $ListAllowedUsers+="$($_.Trustee.Name)," } else { if ($_.AceType -eq 1) { $ListDeniedUsers+="$($_.Trustee.Name),"}}  }
   $writer.WriteLine($(encode $k0 $k1 $k2 $k3 "$($_.name):$(GetLocation $path):$(GetDriverName $path):$($ListAllowedUsers):$($ListDeniedUsers):$(GetDuplex $path):$(GetColor $path)::::$([System.Int16](GetA3 $path))"  ))
   $writer.Flush()
  }  
 }
 $writer.Dispose()
 $stream.Dispose()
 $client.close()
}
$listener.stop()

