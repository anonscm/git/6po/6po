#!/usr/bin/perl
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

use strict;
use warnings;
use Socket;
use Encode qw( decode_utf8 encode_utf8 );
use YAML::Tiny;

my $yaml = YAML::Tiny->read('ucollector.yml');
my $CUPS_DIR=$yaml->[0]->{cups}->{dir};
my $CUPS_PPD_DIR=$CUPS_DIR."ppd/";
my $printers_file=$CUPS_DIR."printers.conf";

sub encode { 
	my ($rep,$k0,$k1,$k2,$k3) = @_;
	my $result="";
	encode_utf8($rep);
	my $s_bytes=unpack "H*",$rep;
	my @bytes = map hex, $s_bytes =~ /../g;
 	my $r=8-($#bytes%8);
	for (my $i = 0; $i < $r; $i++) { push (@bytes, 0); }
	for (my $i = 0; $i <= ($#bytes-8) ; $i+=8) { 
    	my $v0=$bytes[$i+3] + ( $bytes[$i+2]<<8 )+ ($bytes[$i+1]<<16 ) + ($bytes[$i]<<24);
    	my $v1=$bytes[$i+7] + ( $bytes[$i+6]<<8 )+ ($bytes[$i+5]<<16 ) + ($bytes[$i+4]<<24);
		my $sum = 0; 
		my $n = 0;
		while ($n<32) {
			$sum += 0x9e3779b9;
			$v0 += ((($v1<<4)+$k0) ^ ($v1+$sum) ^ ((0x07FFFFFF & ($v1>>5))+$k1));
			$v0 &= 0xFFFFFFFF;
			$v1 += ((($v0<<4)+$k2) ^ ($v0+$sum) ^ ((0x07FFFFFF & ($v0>>5))+$k3));
			$v1 &= 0xFFFFFFFF;
			$n++;
		}
		$result.=sprintf("%08x%08x",$v0,$v1);
	}
	return $result;
}

sub analyse_ppd {
	my ($ppd_file)=@_;
	$ppd_file=$CUPS_PPD_DIR.$ppd_file.".ppd";
	open(PPD_FILE,"<$ppd_file") || die "Erreur de lecture $ppd_file, Erreur: $!";
	my $etat=0;
	my $color=0;
	my $color_c="";
	my $color_nb="";
	my $color_option="";
	my $duplex=0;
	my $option_duplex="";
	my $a3=0;
	my $option;
	my $choix;
	while (<PPD_FILE>) {
		chomp($_);
		if (/^\*ColorDevice:\sTrue/) {
			$color=1;
		}
		elsif (/^\*PageSize A3\/A3/) {
			$a3=1;
		}
		elsif ((/^\*OpenUI/) || (/^\*JCLOpenUI/)) {
			$option=$_;
			$option=~s/^.*OpenUI \*([^\/:]*)[\/:].*/$1/;
			if ($option eq "Duplex" || $option eq "KMDuplex") {
				$duplex=1;
			}			
			elsif (($option eq "SelectColor") || ($option eq "ColorModel")) {
				if ($color>0) {
					$color_option=$option;
					$etat=1;
				}
			}
			elsif ($option eq "OptionDuplex") {
				$etat=2;
			}
		}
		elsif (/^\*UIConstraints:\s\*Duplex[^\*]*\*Option[^\s]*\sFalse/) {
			$option_duplex=$_;
			$option_duplex=~s/^\*UIConstraints:\s\*Duplex[^\*]*\*(Option[^\s]*)\sFalse/$1/;
		}
		elsif ($etat>0) {
			if ((/^\*CloseUI/) || (/^\*CloseJCLOpenUI/)) {
				$etat=0;
			}
			elsif ($etat==1) {
				if (/^\*$option/) {
					$choix=$_;
					$choix=~s/\*$option ([^\/]*)\/.*/$1/;
					if (($choix eq "CMY") || ($choix eq "RGB") || ($choix eq "CMYK") || ($choix eq "Color") || ($choix eq "CMYKImageRET2400")) {
						$color_c=$choix;
						#$color++;
					}
					elsif ($choix=~m/Gray/) {
						$color_nb=$choix;
						#$color++;
					}
				}
			}			
			elsif ($etat==2) {
				if (/^Default$option:\sTrue/) {
					$duplex=1;
				}
			}
		}	
	}
	close(PPD_FILE);
	
	if (($duplex==1) && ($option_duplex ne "")) {
		open(PPD_FILE,"<$ppd_file") || die "Erreur de lecture $ppd_file, Erreur: $!";
		while (<PPD_FILE>) {
			if (/^\*Default$option_duplex:/) {
				if (/False/) {
					$duplex=0;
				}	
				last;
			}
		}
		close(PPD_FILE);
	}

	return "$duplex:$color:$color_option:$color_c:$color_nb:$a3";
}

my $proto = getprotobyname('tcp');

socket(SOCKET, PF_INET, SOCK_STREAM, $proto)
   or die "Impossible d'ouvrir le socket $!\n";
setsockopt(SOCKET, SOL_SOCKET, SO_REUSEADDR, 1)
   or die "Can't set socket option to SO_REUSEADDR $!\n";

# bind to a port, then listen
bind( SOCKET, pack_sockaddr_in($yaml->[0]->{service}->{port}, inet_aton($yaml->[0]->{service}->{ip})))
   or die "Impossible d'écouter sur le port $yaml->[0]->{service}->{port}! \n";

listen(SOCKET, 5) or die "listen: $!";

local $SIG{'PIPE'} = 'IGNORE';

my $etat=0;
my $ligne;
my $allowusers;
my $denyusers;
my $printer;
my $location;
my $info;
my $ppd_file;
my $TEA=$yaml->[0]->{tea};


while (my $client = accept(NEW_SOCKET, SOCKET)) {
	print NEW_SOCKET encode("START",$TEA->{k0},$TEA->{k1},$TEA->{k2},$TEA->{k3})."\n";
	open(PRINTERS_FILE,"<$printers_file") || die "Erreur de lecture $printers_file, Erreur: $!";
	while (<PRINTERS_FILE>) {
		chomp($_);
		if ($etat==1) {
			if (/^Location/) {
				$location=$_;
				$location=~s/Location (.*)$/$1/;
			}
			if (/^Info/) {
				$info=$_;
				$info=~s/Info (.*)$/$1/;
			}
			elsif (/^AllowUser/) {
				my $allowuser=$_;
				$allowuser=~s/AllowUser [@]{0,1}(.*)$/$1/;
				$allowusers.="$allowuser,";
			}
			elsif (/^DenyUser/) {
				my $denyuser=$_;
				$denyuser=~s/DenyUser [@]{0,1}(.*)$/$1/;
				$denyusers.="$denyuser,";
			}
			elsif (/^<.Printer>$/) {
				if (-f $ppd_file) {
					if ($allowusers eq "") {
						$allowusers="Tout le monde,";
					}
					$ligne="$printer:$location:$info:$allowusers:$denyusers:".analyse_ppd($printer);
					print NEW_SOCKET encode($ligne,$TEA->{k0},$TEA->{k1},$TEA->{k2},$TEA->{k3})."\n";
				}
				$etat=0;
			}
		}
		elsif (/^<Printer/ || /^<DefaultPrinter/) {
				$ligne="";
				$allowusers="";
				$denyusers="";
				$printer=$_;
				if (/^<Printer/) {
					$printer=~s/^<Printer (.*)>$/$1/;
				} else {
					$printer=~s/^<DefaultPrinter (.*)>$/$1/;
				}
				$ppd_file=$CUPS_PPD_DIR.$printer.".ppd";
				$etat=1;
		}
	}
	close(PRINTERS_FILE);
	close(NEW_SOCKET);
}
close(SOCKET);
