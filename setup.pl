#!/usr/bin/perl
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

use strict;
use warnings;
use YAML::Tiny;
use File::Path qw(make_path remove_tree);
use Getopt::Long;
use Text::Template;
use DBI;
use POSIX qw(strftime);


my $yaml = YAML::Tiny->read('6po.yml') or die "Impossible d'ouvrir le fichier 6po.yml" ;
my $SISPO = $yaml->[0]->{sispo};
my $APACHE = $yaml->[0]->{apache};
my $SISPODAV = $yaml->[0]->{sispodav};
my $SISPOWEB = $yaml->[0]->{sispoweb};
my $SISPOMAIL = $yaml->[0]->{sispomail};
my $LDAP = $yaml->[0]->{ldap};
my $KERBEROS = $yaml->[0]->{kerberos};
my $SERVERS = $yaml->[0]->{servers};
my $TEA = $yaml->[0]->{tea};
my $POSTFIX = $yaml->[0]->{postfix};
my $CUPS = $yaml->[0]->{cups};
my $CRON = $SISPO->{cron};
my $MISC = $yaml->[0]->{misc};
my $FAIL2BAN = $MISC->{fail2ban};

sub makeFileFromTemplate {
	my ($hash,$tmpl,$filename)=@_;
	my $template = Text::Template->new(SOURCE => "templates/$tmpl") || die "Impossible d'ouvrir le fichier $tmpl";
	my $result = $template->fill_in(HASH => \%{$hash});
	if (defined $result) { 
		print ("Création du fichier $filename\n");
		open(my $fh, ">", $filename);
		print $fh $result;
		close($fh);
	}
	else { die "Impossible de remplir le template: $Text::Template::ERROR" }
}

sub addToFileAndLaunchCommand {
	my ($filename,$reflines,$command,$nobackup)=@_;
	my @lines=@{$reflines};
	my $found=0;
	if ( -f $filename ) {
		my $now = strftime "%Y%m%d%H%M%S", localtime;
		my $backup=${filename}."_".$now;
		print "Sauvegarde de $filename en $backup\n";
		system("cp -p $filename ${filename}_$now");
		print "Modification du fichier $filename\n";
		open(my $fh_src, "<", $backup);
		open(my $fh_dst, ">", $filename);
		while (<$fh_src>) {
  		if ($_ eq "#<6PO>\n") {
				$found=1;	 
			}
			if (! $found) {
				print $fh_dst "$_";
			}
			if ($_ eq "#</6PO>\n") {
				$found=0;	 
  		}
	}
	close($fh_src);
	print $fh_dst "#<6PO>\n";
	for my $line (@lines) {
		print $fh_dst "$line\n";
	}
	print $fh_dst "#</6PO>\n";
	close($fh_dst);
	if ($command) {
		system($command);
	}
	if ($nobackup) {
		print "suppresion du fichier $backup\n";
	}
	} else {
		print "Le fichier $filename n'existe pas.\n";
	}
}

mkdir ("/etc/6po");
symlink($SISPO->{dir}."6po.yml","/etc/6po/6po.yml");
symlink($SISPO->{dir}."6pod.sh","/etc/init.d/6pod.sh");
system("cp -p $SISPO->{dir}bin/html2pdf.sh /usr/local/bin/html2pdf.sh");
system("cp -p $SISPO->{dir}bin/pdfconvertme.pl /usr/local/bin/pdfconvertme.pl");
system("patch -p0 /usr/local/bin/pdfconvertme.pl < $SISPO->{dir}patches/pdfconvertme.patch");

if (! -e $SISPOWEB->{dir}) { system("cp -rp $SISPO->{dir}/www/6poweb $SISPOWEB->{dir}"); }

my $option_apache="";
my $option_cups="";
my $option_postfix="";
my $option_fail2ban="";
my $full="";
my $erasedb="";
my $nobackup="";

GetOptions ('full' => \$full, 'erasedb' => \$erasedb, 'apache' => \$option_apache,'cups' => \$option_cups,'postfix' => \$option_postfix,'fail2ban'=> \$option_fail2ban, 'nobackup' => \$nobackup);

# Configuration de fail2ban (à faire avant celle d'Apache si traitement d'une blacklist fail2ban par Apache)
if ($FAIL2BAN->{active} && (-d "/etc/fail2ban") && ($option_fail2ban || $full) && ($SISPODAV->{active} || ($SISPOWEB->{auth} eq "ldap"))) {
	my %fail2ban;
	if ($SISPODAV->{active} && ($SISPOWEB->{auth} eq "ldap")) { $fail2ban{"logfile"}="6po*_error.log"; }
	elsif ($SISPODAV->{active}) { $fail2ban{"logfile"}="6podav_error.log"; } 
	else { $fail2ban{"logfile"}="6poweb_error.log"; } 
	$fail2ban{"maxretry"}=$FAIL2BAN->{maxretry};
	$fail2ban{"findtime"}=$FAIL2BAN->{findtime};
	$fail2ban{"bantime"}=$FAIL2BAN->{bantime};
	if ($FAIL2BAN->{apache_blacklist}) { 
		$fail2ban{"action"}="6po_blacklist"; 
		system("cp $SISPO->{dir}fail2ban/banned.conf /etc/apache2/sites-available/");
	}
	else { $fail2ban{"action"}='iptables-multiport[name=6po, port="http,https", protocol=tcp]'; }
	makeFileFromTemplate(\%fail2ban,"fail2ban.tmpl","/etc/fail2ban/jail.conf");	
	system("cp $SISPO->{dir}fail2ban/6po.conf /etc/fail2ban/filter.d/");
	system("cp $SISPO->{dir}fail2ban/6po_blacklist.conf /etc/fail2ban/action.d/");
	system("/etc/init.d/fail2ban restart");
}		

# Configuration Apache :
#	- création et activation des virtualhosts 
#	- activation des modules 

if ($option_apache || $full) {
	print "le fichier $APACHE->{SSLCertificateFile} n'existe pas"  unless -e $APACHE->{SSLCertificateFile} ;
	print "le fichier $APACHE->{SSLCertificateKeyFile} n'existe pas"  unless -e $APACHE->{SSLCertificateKeyFile};
	print "le fichier $APACHE->{SSLCertificateChainFile} n'existe pas"  unless -e $APACHE->{SSLCertificateChainFile};
	open(my $ports,">","/etc/apache2/ports.conf");
	print $ports "NameVirtualHost $APACHE->{sispoweb}->{ip}:80\n";
	print $ports "Listen 80\n";
	print $ports "NameVirtualHost $APACHE->{sispoweb}->{ip}:443\n";
	print $ports "Listen 443\n";
	if ($SISPODAV->{active} && !($APACHE->{sispodav}->{ip} eq $APACHE->{sispoweb}->{ip})) {
		print $ports "NameVirtualHost $APACHE->{sispodav}->{ip}:80\n";
		print $ports "Listen 80\n";
		print $ports "NameVirtualHost $APACHE->{sispodav}->{ip}:443\n";
		print $ports "Listen 443\n";
	}
	close($ports);
	my %virtuals;
	unlink glob ("/etc/apache2/sites-enabled/6po_*");
	$virtuals{"ServerIP"}=$APACHE->{sispodav}->{ip};
	$virtuals{"ServerName"}=$APACHE->{sispodav}->{serverName};
	$virtuals{"ServerAdmin"}=$APACHE->{sispodav}->{serverAdmin};
	$virtuals{"Directory"}=$SISPODAV->{dir};
	chop($virtuals{"Directory"});
	$virtuals{"AuthLDAPURL"}=$LDAP->{uri}."/".$LDAP->{user_base_dn}."?";
	if ($LDAP->{type} eq "ad") {
		$virtuals{"AuthLDAPURL"}.="samAccountName";
		$virtuals{"AuthLDAPGroupAttribute"}="member";
		$virtuals{"AuthLDAPGroupAttributeIsDN"}="on";
	} else {
		$virtuals{"AuthLDAPURL"}.="uid";
		$virtuals{"AuthLDAPGroupAttribute"}="memberuid";
		$virtuals{"AuthLDAPGroupAttributeIsDN"}="off";
	}
	if ($LDAP->{anonymous}) { 
		$virtuals{"AuthLDAPBindDN"}="#AuthLDAPBindDN";
		$virtuals{"AuthLDAPBindPassword"}="#AuthLDAPBindPassword";
	} 
	else {	
		$virtuals{"AuthLDAPBindDN"}="AuthLDAPBindDN ".$LDAP->{bind_user};
		$virtuals{"AuthLDAPBindPassword"}='AuthLDAPBindPassword "'.$LDAP->{bind_password}.'"';
	}
	$virtuals{"SISPO_DIR"}=$SISPO->{dir};
	$virtuals{"SSLCertificateFile"}=$APACHE->{SSLCertificateFile};
	$virtuals{"SSLCertificateKeyFile"}=$APACHE->{SSLCertificateKeyFile};
	$virtuals{"SSLCertificateChainFile"}=$APACHE->{SSLCertificateChainFile}; 
	$virtuals{"fail2ban"}="#";
	if ($SISPODAV->{active}) {
		if ($FAIL2BAN->{active} && $FAIL2BAN->{apache_blacklist}) { $virtuals{"fail2ban"}=""; }
		makeFileFromTemplate(\%virtuals,"6podav.tmpl","/etc/apache2/sites-enabled/6podav.conf");
		makeFileFromTemplate(\%virtuals,"6po80.tmpl","/etc/apache2/sites-enabled/6podav80.conf");
	}
	
	$virtuals{"ServerIP"}=$APACHE->{sispoweb}->{ip};
	$virtuals{"ServerName"}=$APACHE->{sispoweb}->{serverName};
	$virtuals{"ServerAdmin"}=$APACHE->{sispoweb}->{serverAdmin};
	$virtuals{"Directory"}=$SISPOWEB->{dir};
	$virtuals{"fail2ban"}="#";
	if ($SISPOWEB->{auth} eq "ldap") {
		$virtuals{"ldap_auth"}="";
		$virtuals{"cas_auth"}="#";
		if ($FAIL2BAN->{active} && $FAIL2BAN->{apache_blacklist}) { $virtuals{"fail2ban"}=""; }
	} else {
		$virtuals{"ldap_auth"}="#";
		$virtuals{"cas_auth"}="";
	}
	chop($virtuals{"Directory"});
	makeFileFromTemplate(\%virtuals,"6poweb.tmpl","/etc/apache2/sites-enabled/6poweb.conf");
	makeFileFromTemplate(\%virtuals,"6po80.tmpl","/etc/apache2/sites-enabled/6poweb80.conf");
	
	my @modules=("php*","ssl","rewrite");
	if ($SISPOWEB->{auth} eq "cas") { push @modules, "auth_cas"; }
	if ($SISPOWEB->{auth} eq "ldap" || $SISPODAV->{active}) { push @modules, ("authnz_ldap","ldap"); }  
	if ($SISPODAV->{active}) { push @modules, ("dav","dav_fs"); }
	for my $module (@modules) {
		if (!(-f "/etc/apache2/mods-enabled/$module.load")) {
			system ("a2enmod $module");
		}
	}
	system($APACHE->{restart});
}

# Création du fichier de configuration pour 6poweb
my %php;
$php{"ldap_uri"}=$LDAP->{uri};
$php{"ldap_type"}=$LDAP->{type};
$php{"ldap_anonymous"}=$LDAP->{anonymous};
$php{"ldap_user_dn"}=$LDAP->{bind_user};
$php{"ldap_user_pwd"}=$LDAP->{bind_password};
$php{"ldap_base_dn"}=$LDAP->{base_dn};
$php{"ldap_user_base_dn"}=$LDAP->{user_base_dn};
$php{"ldap_group_base_dn"}=$LDAP->{group_base_dn};
$php{"nested_group"}=$LDAP->{nested_group};
$php{"cache_group"}=$SISPOWEB->{cache_group};
$php{"url_active"}=$SISPOWEB->{url_active};
$php{"sispoweb_tmpdir"}=$SISPOWEB->{tmpdir};
$php{"sispomail_tmpdir"}=$SISPOMAIL->{tmpdir};
$php{"sispomail_active"}=$SISPOMAIL->{active};
my $servers_ip="";
foreach my $server (keys(%{$SERVERS})) {
  my $SERVER=$SERVERS->{$server};
	if (! defined $SERVER->{"site"}) { $servers_ip.=sprintf('"%s"=>"_default_",',$SERVER->{"ip"}); }
	else { $servers_ip.=sprintf('"%s"=>"%s",',$SERVER->{"ip"},$SERVER->{"site"}); }
}
chop($servers_ip);
$php{"servers_ip"}=$servers_ip;
makeFileFromTemplate(\%php,"php.tmpl",$SISPOWEB->{dir}."include/conf.php");

# Création des répertoires temporaires
make_path($SISPODAV->{tmpdir},{owner=>'www-data', group=>'www-data',mode => 0755});
make_path($SISPOWEB->{tmpdir},{owner=>'www-data', group=>'www-data',mode => 0755});
make_path($SISPOMAIL->{tmpdir},{owner=>'www-data', group=>'www-data',mode => 0755});
make_path($SISPO->{logdir});

# Création des fichiers de configuration pour les collecteurs
# et renseignement du serveur d'impression cups (local pour windows)
my %collector;
$collector{"k0"}=$TEA->{k0};
$collector{"k1"}=$TEA->{k1};
$collector{"k2"}=$TEA->{k2};
$collector{"k3"}=$TEA->{k3};

foreach my $server (keys(%{$SERVERS})) {
	my $SERVER=$SERVERS->{$server};
		$collector{"port"}=$SERVER->{sisport};
		$collector{"ip"}=$SERVER->{ip};
		$collector{"domain"}=$LDAP->{base_dn};
		$collector{"domain"}=~s/^dc=([^,]*),.*/$1/;
		makeFileFromTemplate(\%collector,"ucollector.tmpl","servers/ucollector.yml");
		makeFileFromTemplate(\%collector,"wcollector.tmpl","servers/wcollector.csv");
}

# Création du fichier pour la rotation des logs de 6PO
my %logrotate;
$logrotate{"logdir"}=$SISPO->{logdir};
makeFileFromTemplate(\%logrotate,"logrotate.tmpl","/etc/logrotate.d/6po");
system("logrotate -f /etc/logrotate.conf");

# Création du fichier de conf pour le démon 6PO
my %init;
$init{"sispodir"}=$SISPO->{dir};
if ($KERBEROS->{active}) {
	$init{"k5start"}="yes";
	$init{"keytab"}=$KERBEROS->{keytab};
	$init{"principal"}=$KERBEROS->{principal};
	$init{"realm"}=$KERBEROS->{realm};
	$init{"ticket"}=$KERBEROS->{ticket};
} 
else {
	$init{"k5start"}="no";
	$init{"keytab"}="";
	$init{"suexec_user"}="";
	$init{"realm"}="";
	$init{"ticket"}="";
}
makeFileFromTemplate(\%init,"6pod.tmpl","/etc/6po/6pod");

# Configuration de kerberos (si serveur distant CUPS nécessite une authentification kerberos)
my %kerberos;
$kerberos{"realm"}=uc($KERBEROS->{realm});
makeFileFromTemplate(\%kerberos,"krb5.tmpl","/etc/krb5.conf");

# Configuration du cups local (si serveur impression sous Windows)
my @lines;
if ($option_cups || $full) {
	@lines=("ErrorPolicy ".$CUPS->{ErrorPolicy});
	if ((defined $CUPS->{JobRetryInterval}) && (defined $CUPS->{JobRetryLimit}) ) {
		push @lines,"JobRetryInterval ".$CUPS->{JobRetryInterval};
		push @lines,"JobRetryLimit ".$CUPS->{JobRetryLimit};
	}
	if (defined $CUPS->{MaxClients}) {
		push @lines,"MaxClients ".$CUPS->{MaxClients};
		push @lines,"MaxClientsPerHost ".$CUPS->{MaxClients};
	}
	addToFileAndLaunchCommand("/etc/cups/cupsd.conf",\@lines,$CUPS->{restart},$nobackup);
	system("cp $SISPO->{dir}/6PO_PDF.PPD /usr/share/cups/model/");
}

# Configuration de postfix
if ($SISPOMAIL->{active} && ($option_postfix || $full)) {
	my %main;
	$main{"myhostname"}=$POSTFIX->{myhostname};
	makeFileFromTemplate(\%main,"main.cf.tmpl","/etc/postfix/main.cf");
	makeFileFromTemplate(\%main,"header_checks.tmpl","/etc/postfix/header_checks");
	@lines=("6po\tunix\t-\tn\tn\t-\t-\tpipe","\tflags=Xhq\tuser=www-data\targv=$SISPO->{dir}6pomail.pl");
	addToFileAndLaunchCommand("/etc/postfix/master.cf",\@lines,$POSTFIX->{restart},$nobackup);
	@lines=("6po: www-data");
	addToFileAndLaunchCommand($POSTFIX->{aliases},\@lines,"newaliases",$nobackup);
}

# Configuration de la fstab pour partage DAV monté en bindfs
if ($SISPODAV->{active}) {
	open(BINFS,"bindfs --version |");
	my @version=split / /, <BINFS>;
	if ($version[1] lt "1.12") {    
        	@lines=("$SISPODAV->{dir}\t$SISPODAV->{dir}\tfuse.bindfs\towner=www-data,group=www-data,perms=uf=w,nonempty\t0\t0");
	} else {
        	@lines=("$SISPODAV->{dir}\t$SISPODAV->{dir}\tfuse.bindfs\tforce-user=www-data,force-group=www-data,perms=uf=w,nonempty\t0\t0");
	}	       
	addToFileAndLaunchCommand("/etc/fstab",\@lines,"umount $SISPODAV->{dir} ; mount $SISPODAV->{dir}",$nobackup);
}

# Configuration des tâches planifiées
my %cron;
my $crontab="/tmp/crontab";
$cron{"sispomail_tmpdir"}=$SISPOMAIL->{tmpdir};
$cron{"sispodav_tmpdir"}=$SISPODAV->{tmpdir};
$cron{"sispoweb_tmpdir"}=$SISPOWEB->{tmpdir};
$cron{"remain"}=$CRON->{remain};
mkdir ($SISPO->{dir}."cron");
my $clean=$SISPO->{dir}."cron/clean.sh";
makeFileFromTemplate(\%cron,"clean.tmpl",$clean);
chmod 0755, $clean;
my $cronlog=$SISPO->{logdir}."cron.log";
@lines=("*/5 * * * * $clean >> $cronlog 2>&1 ");
if (defined $CRON->{collector}) {
	push @lines,"$CRON->{collector} cd $SISPO->{dir} && $SISPO->{dir}"."collector.pl >> $cronlog 2>&1";
}
if (defined $CRON->{collector_full}) {
	push @lines,"$CRON->{collector_full} cd $SISPO->{dir} && $SISPO->{dir}"."collector.pl --full >> $cronlog 2>&1";
}
system("crontab -l > ".$crontab);
addToFileAndLaunchCommand($crontab,\@lines,"crontab $crontab",$nobackup);

# Création de la base de données locale sqlite
my $dbfile = "6po.db";
if ((! -f $dbfile) || $erasedb) { 
	my $dsn = "dbi:SQLite:dbname=$dbfile";
	if (-f $dbfile) {
		unlink($dbfile);
	}
	
	my $dbh = DBI->connect($dsn);
	my $sql = <<'END_SQL';
CREATE TABLE changes (
  printer varchar(255),
  site varchar(15),
  old_allowusers varchar(255),
  new_allowusers varchar(255),
  old_denyusers varchar(255),
  new_denyusers varchar(255),
  PRIMARY KEY (printer,site)
)
END_SQL
	$dbh->do($sql);
	$sql = <<'END_SQL';
CREATE TABLE printers (
  printer varchar(255),
  site varchar(15),
  location varchar(255),
  info varchar(255),
  allowusers varchar(255),
  denyusers varchar(255),
  davpath varchar(255),
  duplex tinyint(1),
  color tinyint(1),
  color_option varchar(255),
  color_c varchar(255),
  color_nb varchar(255),
	a3 tinyint(1),
  insertedat datetime,
  lastseen datetime,
  PRIMARY KEY (printer,site)
);
END_SQL
	$dbh->do($sql);
	$sql = <<'END_SQL';
CREATE TRIGGER trigger_allowusers AFTER UPDATE ON printers
  WHEN NEW.location<>OLD.location OR NEW.allowusers<>OLD.allowusers OR NEW.denyusers<>OLD.denyusers OR NEW.duplex<>OLD.duplex OR NEW.color<>OLD.color
  BEGIN
      INSERT INTO changes(site,printer,old_allowusers,new_allowusers,old_denyusers,new_denyusers) VALUES (NEW.site,NEW.printer,OLD.allowusers,NEW.allowusers,OLD.denyusers,NEW.denyusers);
  END 
END_SQL
	$dbh->do($sql);
}
