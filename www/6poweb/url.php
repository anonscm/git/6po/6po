<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of 6PO.
 * 
 * 6PO is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 6PO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 6PO; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de 6PO.
 * 
 * 6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * 6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * 6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */

include "include/conf.php";

$url=$_POST["url"];
if ((substr($url,0,7)=="http://" || (substr($url,0,8)=="https://")) && filter_var($url, FILTER_VALIDATE_URL) !== false) {
	$tmpdir=$sispoweb_tmpdir.$_SERVER["REMOTE_USER"]."/";
	if (!file_exists($tmpdir)) mkdir($tmpdir); 
	$filename=time().".url";
	file_put_contents($tmpdir.$filename,$url);
	$fifpo="/tmp/fifpo";
	if (!file_exists($fifpo))
		echo "communication avec le serveur impossible !";
	elseif (file_put_contents($fifpo,"$_SERVER[REMOTE_ADDR] $_SERVER[REMOTE_USER] $_POST[printer] $_POST[server_ip] $_POST[couleur] $_POST[duplex] $_POST[copies] $_POST[ppp] $_POST[range] $_POST[format] $_POST[orientation] $tmpdir $filename\n"))
		echo "OK";
	else 
		echo "erreur interne !";
} else {
	echo "URL non valide !";
}
?>
