<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of 6PO.
 * 
 * 6PO is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 6PO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 6PO; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de 6PO.
 * 
 * 6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * 6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * 6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */

include("include/ldap.php");
include("include/conf.php");

session_start();

$login=$_SERVER['REMOTE_USER'];
if ($cache_group) {
	if (!isset($_SESSION["tab_perms"])) {
		$_SESSION["tab_perms"]=createTabPerms($login,$ldap_uri,$ldap_type,$ldap_base_dn,$ldap_anonymous,$ldap_user_dn,$ldap_user_pwd,$ldap_user_base_dn,$ldap_group_base_dn,$nested_group);
	}
	$tab_perms=$_SESSION["tab_perms"];
} else {
	$tab_perms=createTabPerms($login,$ldap_uri,$ldap_type,$ldap_base_dn,$ldap_anonymous,$ldap_user_dn,$ldap_user_pwd,$ldap_user_base_dn,$ldap_group_base_dn,$nested_group);
}

if (count($tab_perms)>0) {

	$server_ip=$_GET["server_ip"];
	include("include/printers_$server_ip.php");
	if (isset($_GET["printer"]) && $_GET["printer"]!="" && isset($printers[$_GET["printer"]]))  {
		$tprinters[$_GET["printer"]]=$printers[$_GET["printer"]];
	} else {
		$tprinters=$printers;
	}
	
	print<<<FIN
	
	<form id='upload' action='sendjob.php' method='POST' enctype='multipart/form-data'>
	<input type='hidden' id='printer_visibility' value='all'/>
	<input type='hidden' id='server_ip' value='$server_ip'/>
	<table style='width:100%' id='principal'>
	
FIN;
	
	$nbp=0;
	foreach($tprinters as $printer=>$value) {
		if ( (count(array_intersect($tab_perms,$printers[$printer]['allowusers']))>0) && (count(array_intersect($tab_perms,$printers[$printer]['denyusers']))<1)) {
			print(" <tr><td style='background-color:#ddd;width:300px'><b>$printer</b><input type='hidden' id='printer${nbp}' value='$printer'></td>\n");
			print("  <td style='background-color:#e8e8ff'>".$printers[$printer]['location']."</td></tr>\n");
			print<<<FIN
	 <tr><td><a href='javascript:PrinterVisibility($nbp)'><img src='printer.png'></a></td>
	  <td>
	   <table style='width:100%'>
	    <tr>
	     <td style='width:400px'>
	      <fieldset>
	       <legend>Options d'impression</legend>
	       <table>
	        <tr>
	
FIN;
			if ($printers[$printer]['duplex']==1) {
	      print("       <td>");
				print("        <select id='duplex${nbp}'><option value='recto' SELECTED>recto</option><option value='recto-verso'>recto-verso</option><option value='recto-verso-bord-court'>recto-verso bord court</select>");
				print("       </td>");
			}
			if ($printers[$printer]['color']==1) {
				print("       <td>");
				print("        <select id='couleur${nbp}'><option value='couleur' SELECTED>couleur</option><option value='noir_et_blanc'>noir et blanc</option></select>\n");
				print("       </td>");
			}
			if ($printers[$printer]['a3']==1) {
				print("       <td>");
				print("        <select id='format${nbp}'><option value='a4' SELECTED>A4</option><option value='a3'>A3</option></select>");
				print("       </td>");
			}
			print("       <td>");
			print("        <select id='orientation${nbp}'><option value='portrait' SELECTED>Portrait</option><option value='paysage'>Paysage</option></select>");
			print("       </td>");
			print("       <td style='white-space: nowrap'>Copies : \n");
			print("        <select id='copies${nbp}'>\n");
			print("         <option value=1 SELECTED>1</option>\n");
			for ($i=2;$i<=10;$i++) print ("         <option value=$i>$i</option>\n");	
	print<<<FIN
	          </select>
	         </td>
	         <td style='white-space: nowrap'>Pages/page :
	          <select id='ppp${nbp}'>
	           <option value=1>1</option>
	           <option value=2>2</option>
	           <option value=4>4</option>
	           <option value=6>6</option>
	           <option value=9>9</option>
	           <option value=16>16</option>
	          </select>
	         </td>
	         </td>
	         <td style='white-space: nowrap' colspan=2>Pages :
	          <input type='text' size=10 id='range${nbp}' onBlur='ValidateRange(this)'/>
	         </td>
		</tr>
	       </table>
	      </fieldset>
	     </td>
	     <td></td>
	    </tr>
	    <tr>
	     <td colspan=2>
	      <fieldset>
	       <legend>Fichiers à imprimer</legend>
	       <div>
	         <label for='fileselect'>Sélectionnez vos fichiers:</label>
	         <input type='file' id='fileselect${nbp}' name='fileselect${nbp}[]' multiple='multiple' />
	
FIN;
			if ($sispomail_active) print("<input type='button' value='Email...' onclick='ShowOverlay($nbp)'/>\n");
	         	print("         <div  class='filedrag' id='filedrag${nbp}'>ou déposez-les ici</div>\n");
			if ($url_active) {
				print("         <label for='url'>Collez votre URL:</label>\n");
	        		print("         <input type='text' id='url${nbp}' onpaste='setTimeout(function(){PrintUrl($nbp)}, 500)' size=30>\n");
				print("         <br/>\n");
			}
	print<<<FIN
	       </div>
	      </fieldset>
	     </td>
	    </tr>
	    <tr>
	     <td>
	     </td>
	    </tr>
	    <tr>
	     <td colspan=2>
	      <fieldset>
	       <legend>Fichiers envoyés</legend>
	        <div class='progress' id='progress${nbp}'></div>
	      </fieldset>
	     </td>
	    </tr>
	   </table>
	
FIN;
			$nbp++;
		}
	}
	print("</table>\n");
	
	print("<center>\n");
	if ($nbp<1) {
		print("<center>\n");
		print("<font size=+1>\n");
		include "noprinter.html";
		print("</font>\n");
		print("<br/><img src='6po.png'>\n");
		print("</center>\n");
	} else {
			if (empty($_GET["printer"])) {
				print("<br/><a href='javascript:ListVisibility()'>Voir la liste de l'ensemble des imprimantes 6PO pour ce site</a>, y compris celles pour lesquelles vous n'avez aucun droit d'impression.<br/><br/>");
			}
	}
	$i=0;
	print("<table id='tablist' class='invisible'>\n");
	foreach($tprinters as $printer=>$value) { 
		print("<tr><td style='background-color:".($i%2==0?"#ddd":"#e8e8ff")."'>".$printers[$printer]["location"]);
		print("</td><td style='background-color:".($i%2==0?"#ddd":"#e8e8ff")."'>$printer</td></tr>\n");
		$i++;
	}
	if ($i==0) print("<tr><td style='background-color:#ddd'>Aucune imprimante sur ce serveur !</td></tr>\n");
	print("</table>\n");
	print("</center>\n");
	
	print<<<FIN
	<div id="overlay">
	 <div id="email">
	 </div>
	</div>
	</form>
	</body>
	</html>
FIN;

} else {
	if (isset($_SESSION["tab_perms"])) {
		unset($_SESSION["tab_perms"]);
	}
	print("<center>\n");
	print("<font size=+1>\n");
	include("error.html");
	print("</font>\n");
	print("<br/><img src='6po.png'>\n");
	print("</center>\n");
}

?>
