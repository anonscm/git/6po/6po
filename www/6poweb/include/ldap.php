<?php 

function ldap_connection($ldap_uri) {
	$connect = ldap_connect($ldap_uri);
	ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3); 
	ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
	ldap_set_option($connect, LDAP_OPT_TIMELIMIT, 30);
	return $connect;
}

function createTabPerms($login,$ldap_uri,$ldap_type,$ldap_base_dn,$ldap_anonymous,$ldap_user_dn,$ldap_user_pwd,$ldap_user_base_dn,$ldap_group_base_dn,$nested_group) {
	$tab_perms=Array();
	$tab_perms[0]=$login;
	$tab_perms[1]="Tout le monde";
	$ldap=ldap_connection($ldap_uri); 
	if ($ldap) {
		if ($ldap_anonymous)  $bind=ldap_bind($ldap);
		else $bind=ldap_bind($ldap,$ldap_user_dn,$ldap_user_pwd);
		if ($bind) {
			if ($ldap_type=="ad") {
				if ($nested_group) {
					$filter="(&(objectClass=group)(member:1.2.840.113556.1.4.1941:=CN=$login,$ldap_user_base_dn))";
					$restriction = array("dn");
					$search=ldap_search($ldap, $ldap_base_dn, $filter, $restriction);
					if (ldap_errno($ldap)) return;
					$info=ldap_get_entries($ldap, $search);
					$i=0;
					for (;$i<$info["count"];$i++) $tab_perms[$i+2]=preg_replace("/^CN=([^,]*),.*$/i","$1",$info[$i]["dn"]);
					$filter="(&(objectClass=user)(samaccountname=$login))";
					$restriction = array("primaryGroupId");
					$search=ldap_search($ldap, $ldap_base_dn, $filter, $restriction);
					if (ldap_errno($ldap)) return;
					$info=ldap_get_entries($ldap, $search);
					$primarygroupid=$info[0]["primarygroupid"][0];
				} else {
					$filter="(&(objectClass=user)(samaccountname=$login))";
					$restriction = array("memberOf","primaryGroupId");
					$search=ldap_search($ldap, $ldap_user_base_dn, $filter, $restriction);
					if (ldap_errno($ldap)) return;
					$info=ldap_get_entries($ldap, $search);
					$i=0;
					for (;$i<$info[0]["memberof"]["count"];$i++) $tab_perms[$i+2]=preg_replace("/^CN=([^,]*),.*$/i","$1",$info[0]["memberof"][$i]);
					$primarygroupid=$info[0]["primarygroupid"][0];
				}
				if ($primarygroupid=="513") {
					$tab_perms[$i+2]="Utilisateurs du domaine";
				} else {
					$filter="(objectClass=group)";
					$restriction = array("samaccountname","primaryGroupToken");
					$search=ldap_search($ldap, $ldap_group_base_dn, $filter, $restriction);
					if (ldap_errno($ldap)) return;
					$info = ldap_get_entries($ldap, $search);
					for ($j=0;$j<$info["count"];$j++) {
						if ($info[$j]["primarygrouptoken"][0]==$primarygroupid) {
							$tab_perms[$i+2]=$info[$j]["samaccountname"][0];
							break;
						}
					}
				}
			} else {
				$filter="(&(objectClass=posixGroup)(memberUid=$login))";	
				$restriction = array("cn");
				$search=ldap_search($ldap, $ldap_base_dn, $filter, $restriction);
				if (ldap_errno($ldap)) return;
				$info = ldap_get_entries($ldap, $search);
				for ($i=0;$i<$info["count"];$i++) $tab_perms[$i+2]=$info[$i]["cn"][0];
			}
		} else {
			return;	
		}
		ldap_close($ldap);
	} else {
		return;
	}
	return $tab_perms;
}
