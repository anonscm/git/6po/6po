<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of 6PO.
 * 
 * 6PO is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 6PO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 6PO; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de 6PO.
 * 
 * 6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * 6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * 6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */

include("include/conf.php");

session_start();

$title="6PO";
$show=true;
$printer="";
if (isset($_GET["server_ip"]) && isset($servers_ip[$_GET["server_ip"]])) {
	$server_ip=$_GET["server_ip"];
	if (isset($_GET["printer"]))  {
		$title=$_GET["printer"];
		$printer=$_GET["printer"];
	}
} else {
	if (sizeof($servers_ip)<2) {
		$server_ip=array_shift(array_keys($servers_ip)); 
	} else {
		$show=false;		
	}
}

print<<<FIN
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
  'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='fr' lang='fr'>
<head>
 <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
 <meta http-equiv='Content-Language' content='fr' />
 <title>$title</title>
 <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
 <link rel='stylesheet' href='6poweb.css' media='screen'/>
 <script language='javascript' src='6poweb.js'> </script>
</head>
<body>

FIN;

if (sizeof($servers_ip)>1) {
	print("<center>\n");
	print("<div class='zoom'><p>\n");
	asort($servers_ip);
	foreach($servers_ip as $key=>$value) {	
		if (isset($server_ip) && $server_ip==$key ) 
			print("<span class='unbreakable selected'><img src='site.png'/><br/>$value</span>\n");
		else 
			print("<span class='unbreakable'><a href='index.php?server_ip=$key'><img src='site_nb.png'/><br/>$value</a></span>\n");
	}
	print("</p></div>\n");
	print("</center>\n");
}

if ($show) {
	print("<div id='main'>\n");
  print("<center>\n");
  print("<font size=+1>\n");
	include("wait.html");
  print("<br/><img src='6po-loader.gif'/>\n");
  print("<script>LoadMain('$server_ip','$printer')</script>\n");
  print("</center>\n");
	print("</div>\n");
} else {
	print("<center>\n");
	print("<font size=+1>\n");
	include("choice.html");
	print("</font>\n");
	print("<br/><img src='6po.png'/>\n");
	print("</center>\n");
	print("</div>\n");
}
?>
