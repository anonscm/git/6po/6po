<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of 6PO.
 * 
 * 6PO is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 6PO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 6PO; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de 6PO.
 * 
 * 6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * 6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * 6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */

function isSupported($filename) {
	if (mime_content_type($filename)=="application/pdf") return true;
	if (preg_match("/^image/",mime_content_type($filename))) return true;
	if (preg_match("/^text/",mime_content_type($filename))) return true;
	$ext=array(".doc",".odt",".rtf",".wpd",".xls",".ods",".tsv",".csv",".ppt",".pps",".odp",".vsd",".odg",".xps");
	if (in_array(strtolower(substr($filename,-4)),$ext)) return true;
	$ext4=array(".docx",".dotm",".xlsx",".xlsm",".pptx",".pptm",".ppsx",".opxs");
	if (in_array(strtolower(substr($filename,-5)),$ext4)) return true;
	return false;
}

setlocale(LC_ALL, 'fr_FR.UTF-8');
include "include/conf.php";
$tmpdir=$sispoweb_tmpdir.$_SERVER["REMOTE_USER"]."/";
if (!file_exists($tmpdir)) mkdir($tmpdir); 
$tmpfile=$tmpdir.basename($_FILES['SelectedFile']['name']);
if (!move_uploaded_file($_FILES['SelectedFile']['tmp_name'], $tmpfile)) {
    echo " n'a pas pu être envoyé.";
}
elseif (isSupported($tmpfile)) {
	$fifpo="/tmp/fifpo";
	if (!file_exists($fifpo)) 
		echo "communication avec le serveur impossible !";
	elseif (file_put_contents($fifpo,"$_SERVER[REMOTE_ADDR] $_SERVER[REMOTE_USER] $_POST[printer] $_POST[server_ip] $_POST[couleur] $_POST[duplex] $_POST[copies] $_POST[ppp] $_POST[range] $_POST[format] $_POST[orientation] $tmpdir ".rawurlencode($_FILES['SelectedFile']['name'])."\n"))
		echo "OK";
	else 
		echo "Erreur interne !";
} else
	echo "est dans un format non supporté !";

?>
