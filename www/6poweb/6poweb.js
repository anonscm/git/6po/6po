/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of 6PO.
 * 
 * 6PO is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * 6PO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with 6PO; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2015 DOSI AMU (Frédéric Bloise)
 * 
 * 
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de 6PO.
 * 
 * 6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * 6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * 6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */
/*
Largement inspiré de :
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/

// getElementById
function $id(id) {
	return document.getElementById(id);
}

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	if (e.target.className == "filedrag" && e.type == "dragover") e.target.className = "filedraghover";
	else if (e.target.className == "filedraghover" && (e.type=="dragleave" || e.type=="drop")) e.target.className = "filedrag";
}


// file selection
function FileSelectHandler(e,i) {
	// cancel event and hover styling
	FileDragHover(e);

	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;

	// process all File objects
	for (var j = 0, f; f = files[j]; j++) {
		UploadFile(f,i);
	}

}

// upload files
function UploadFile(file,i) {
	var printer=$id("printer"+i), 
	couleur=$id("couleur"+i),
	duplex=$id("duplex"+i),
	copies=$id("copies"+i),
	ppp=$id("ppp"+i),
	range=$id("range"+i),
	format=$id("format"+i),
	orientation=$id("orientation"+i),
	server_ip=$id("server_ip");

	var xhr = new XMLHttpRequest();
	if (xhr.upload && file.size) {

		// create progress bar
		var o = $id("progress"+i);
		var progress = o.appendChild(document.createElement("p"));
		progress.appendChild(document.createTextNode(file.name));


		// progress bar
		xhr.upload.addEventListener("progress", function(e) {
			var pc = parseInt(100 - (e.loaded / e.total * 100));
			progress.style.backgroundPosition = pc + "% 0";
		}, false);

		// file received/failed
		xhr.onreadystatechange = function(e) {
			if (xhr.readyState == 4) {
				if (xhr.responseText!="OK") {
					progress.className = "failed";
					progress.innerHTML=progress.innerHTML+" "+xhr.responseText;
				}
				else {
					progress.className =  "success" ;
				}	
			}
		}

		// start upload
		var data = new FormData();
		data.append('SelectedFile',file);
		data.append('printer',printer.value);
		data.append('server_ip',server_ip.value);
		if (duplex!=null) data.append('duplex',duplex[duplex.selectedIndex].value);
		else data.append('duplex','recto');
		if (couleur!=null) data.append('couleur',couleur[couleur.selectedIndex].value);
		else data.append('couleur','couleur');
		data.append('copies',copies[copies.selectedIndex].value);
		data.append('ppp',ppp[ppp.selectedIndex].value);
		if (range.value!="" && range.className!="wrong-range")  data.append('range',range.value);
		else data.append('range','-');
		if (format!=null) data.append('format',format[format.selectedIndex].value);
		else data.append('format','a4');
		data.append('orientation',orientation[orientation.selectedIndex].value);
		xhr.open("POST", $id("upload").action, true);
		xhr.send(data);

	}

}


// initialize
function Init(i) {

	var fileselect = $id("fileselect"+i),
		filedrag = $id("filedrag"+i),
		submitbutton = $id("submitbutton"+i);

	// file select
	fileselect.addEventListener("change", function(e) { FileSelectHandler(e,i); }, false);

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {

		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", function(e) { FileSelectHandler(e,i); }, false);
		filedrag.style.display = "block";
		
	}

}

/*function PrinterVisibility(i) {
	var v=$id("printer_visibility"),
		p=$id("principal"),
		c=(v.value=="all"?"invisible":"");

	for (var j=0;j<p.rows.length;j++) {
		if (j!=2*i && j!=2*i+1) {
			p.rows[j].className = c;
		}
	}

	v.value=(v.value=="all"?"uniq":"all");
}*/

function PrinterVisibility(i) {
	var p=$id("printer"+i),
		t=$id("principal"),
		s=$id("server_ip");
	window.location=(t.rows.length>2?"index.php?server_ip="+s.value+"&printer="+p.value:"index.php?server_ip="+s.value);
}	

function ListVisibility() {
	var t=$id("tablist");
	t.className = "";
}	

function ShowOverlay(i) {
	var xhr;
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4){
			var o = $id("overlay"), e = $id("email");
			e.innerHTML=xhr.responseText;
			o.style.visibility = "visible";
		}
	}
	var data = new FormData();
	data.append('action','show');
	data.append('index',i);
	xhr.open("POST", "6pomail.php", true);
	xhr.send(data);
}

function realname(filename) {
	return filename.split('/').reverse()[0];
}

function PrintMailsSub(i,fichier) {
	var xhr,
	printer=$id("printer"+i), 
	couleur=$id("couleur"+i),
	duplex=$id("duplex"+i),
	copies = $id("copies"+i),
	ppp = $id("ppp"+i),
	range = $id("range"+i),
	format=$id("format"+i),
	orientation=$id("orientation"+i),
	server_ip=$id("server_ip");

	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4){
			var o = $id("progress"+i);
			var progress = o.appendChild(document.createElement("p"));
			progress.appendChild(document.createTextNode(realname(fichier)));
			if (xhr.responseText!="OK") {
				progress.className = "failed";
				progress.innerHTML=progress.innerHTML+" "+xhr.responseText;
			}
			else {
				progress.className =  "success" ;
			}	
		}
	}

	var data = new FormData();
	data.append('action','print');
	data.append('fichier',fichier);
	data.append('printer',printer.value);
	data.append('server_ip',server_ip.value);
	if (duplex!=null) data.append('duplex',duplex[duplex.selectedIndex].value);
	else data.append('duplex','recto');
	if (couleur!=null) data.append('couleur',couleur[couleur.selectedIndex].value);
	else data.append('couleur','couleur');
	data.append('copies',copies[copies.selectedIndex].value);
	data.append('ppp',ppp[ppp.selectedIndex].value);
	if (range.value!="" && range.className!="wrong-range") data.append('range',range.value);
	else data.append('range','-');
	if (format!=null) data.append('format',format[format.selectedIndex].value);
	else data.append('format','a4');
	data.append('orientation',orientation[orientation.selectedIndex].value);
	xhr.open("POST", "6pomail.php", true);
	xhr.send(data);
}

function PrintMails(i) {
	var fichiers = $id("fichiers[]"),
		o = $id("overlay");
	for(var j=0;j<fichiers.options.length; j++) {
		if(fichiers.options[j].selected == true) { 
			PrintMailsSub(i,fichiers.options[j].value);
		}
	}	
	o.style.visibility = "hidden";
}

function DeleteMails() {
	var xhr,
		o = $id("overlay");
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4){
			o.style.visibility = "hidden";
		}
	}
	var data = new FormData();
	data.append('action','delete');
	xhr.open("POST", "6pomail.php", true);
	xhr.send(data);
}

function CloseOverlay() {
	var o = $id("overlay");
	o.style.visibility = "hidden";
}

function PrintUrl(i) {
	var xhr,
	printer=$id("printer"+i), 
	couleur=$id("couleur"+i),
	duplex=$id("duplex"+i),
	copies = $id("copies"+i),
	ppp = $id("ppp"+i),
	range = $id("range"+i),
	format=$id("format"+i),
	orientation=$id("orientation"+i),
	url = $id("url"+i),
	urlvalue=url.value,
	server_ip=$id("server_ip");

	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4){
			var o = $id("progress"+i);
			var progress = o.appendChild(document.createElement("p"));
			progress.appendChild(document.createTextNode(urlvalue));
			if (xhr.responseText!="OK") {
				progress.className = "failed";
				progress.innerHTML=progress.innerHTML+" "+xhr.responseText;
			}
			else {
				progress.className =  "success" ;
			}	
		}
	}

	var data = new FormData();
	data.append('url',url.value);
	data.append('printer',printer.value);
	data.append('server_ip',server_ip.value);
	if (duplex!=null) data.append('duplex',duplex[duplex.selectedIndex].value);
	else data.append('duplex','recto');
	if (couleur!=null) data.append('couleur',couleur[couleur.selectedIndex].value);
	else data.append('couleur','couleur');
	data.append('copies',copies[copies.selectedIndex].value);
	data.append('ppp',ppp[ppp.selectedIndex].value);
	if (range.value!="" && range.className!="wrong-range") data.append('range',range.value);
	else data.append('range','-');
	if (format!=null) data.append('format',format[format.selectedIndex].value);
	else data.append('format','a4');
	data.append('orientation',orientation[orientation.selectedIndex].value);
	xhr.open("POST", "url.php", true);
	xhr.send(data);

	url.value="";
}

function LoadMain(server_ip,printer) {
	var xhr = new XMLHttpRequest();
	var obj = document.getElementById("main");
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) { 
			obj.innerHTML = xhr.responseText;
			var p=$id("principal");
			if (p !== null) {
				for (var j=0;j<p.rows.length/2;j++) {
					Init(j);
				}
			}	
		}
	}
	xhr.open("GET", "6poweb.php?server_ip="+server_ip+"&printer="+printer);
	xhr.send(null);
}

function ValidRange(range) {
	var etat=0,n1,n2;
	var num=/^[0-9]/;
	if (range=="") return true;
	for (var i=0;i<range.length;i++) {
		switch (etat) {
			case 0 :
				if (num.test(range.charAt(i))) {
					n1=range.charCodeAt(i)-48;
					etat=1;
				} else return false;
				break;
			case 1 :
				if (num.test(range.charAt(i))) n1=n1*10+(range.charCodeAt(i)-48);
				else 
					if (range.charAt(i)=="-") etat=2;
					else 
						if (range.charAt(i)==",") etat=0;
						else return false
				break;
			case 2 :
				if (num.test(range.charAt(i))) {
					n2=range.charCodeAt(i)-48;
					etat=3;
				} else return false;
				break;
			case 3 :
				if (num.test(range.charAt(i))) n2=n2*10+(range.charCodeAt(i)-48);
				else 
					if (n2>n1 && range.charAt(i)==",")  etat=0;
					else return false;
				break;
		}
	} 
	if (etat==1) return true;
	if (etat==3 && n2>n1) return true;
	return 	false;
}

function Refocus(that) {
  	setTimeout(function(){that.focus()}, 10);
}

function ValidateRange(that) {
	if (ValidRange(that.value)) that.className=""; 
	else {
		that.className="wrong-range";
		Refocus(that);
	}
}
