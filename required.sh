#!/bin/bash
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

VERT="\\033[1;32m"
NORMAL="\\033[0;39m"
ROUGE="\\033[1;31m"

function erreur() {
	echo -e "$ROUGE""$1""$NORMAL"
}

function succes() {
	echo -e "$VERT""$1""$NORMAL"
}

function info() {
	echo -e "$BLEU""$1""$NORMAL"
}


echo  "Exécutables requis :"
for required in lpr apache2 postfix sqlite3 bindfs libreoffice convert xpstopdf wkhtmltopdf pdftk
do 
	found=`which $required`
	if [ -z $found ]
	then
		erreur "$required absent"
	else
		succes "$required : $found" 
	fi
done 	

echo 

echo Exécutables requis pour Serveur d impressions Windows :
for required in cupsd
do 
	found=`which $required`
	if [ -z $found ]
	then
		erreur "$required : absent"
	else
		succes "$required : $found" 
	fi
done 	

echo

echo Exécutables requis pour authentification kerberos :
for required in krb5-user kstart
do
	found=`which $required`
	if [ -z $found ]
	then
		erreur "$required : absent"
	else
		succes "$required : $found" 
	fi
done

echo 

echo Modules Perl requis :
for module in Carp DBD::SQLite DBI Digest::SHA Email::MIME Email::MIME::Attachment::Stripper File::Basename File::Copy File::MimeInfo::Magic File::Path File::Pid File::Slurp File::stat File::Temp Getopt::Long HTML::Entities HTML::FromText IO::File IO::Socket IPC::Open2 MIME::Base64 Net::CUPS Net::LDAP POSIX Switch Text::Iconv Text::Markdown Text::Template URI::Encode URI::Escape XML::Feed YAML::Tiny
do
	found=`perl -e "eval {require $module;} or print 'absent'" 2>/dev/null`
	if [ -z $found ]
  then
    succes "$module : OK" 
	else
		erreur "$module : absent"
	fi
done

echo

echo Modules Apache requis :
for module in php* ssl authnz_ldap ldap dav dav_fs rewrite auth_cas
	do
		if [ -f /etc/apache2/mods-available/${module}.load ]
		then
			succes "$module : OK"
		else
			erreur "$module : absent"
		fi
done

echo

echo Module LDAP requis :
if [ -f /etc/php5/mods-available/ldap.ini ] || [ -f /etc/php/7.0/mods-available/ldap.ini ]
then 
	succes "ldap : OK"
else
	erreur "ldap : absent"
fi 

echo

echo Exécutables optionnels :
for required in fail2ban-server
do 
	found=`which $required`
	if [ -z $found ]
	then
		erreur "$required : absent"
	else
		succes "$required : $found" 
	fi
done 	

