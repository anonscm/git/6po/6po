#!/bin/sh
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#
#
# Start/stops 6pod
#
#
### BEGIN INIT INFO
# Provides:          6pod
# Required-Start:    
# Required-Stop: 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: 6pod
# Description:
### END INIT INFO

if [ -f /etc/6po/6pod ] ; then
    . /etc/6po/6pod
else
	exit 0
fi

test -x ${SISPOPATH} || exit 0

if [ "$K5START_START" = "yes" ]
then
	test -x ${K5START_BIN} || exit 0
fi

case "$1" in
	start)
		if [ "$K5START_START" = "yes" ]
		then
			echo -n "Starting K5START: "
			start-stop-daemon --start --pidfile ${K5START_PIDFILE} --exec ${K5START_BIN} -- -b -p ${K5START_PIDFILE} -f ${K5START_KEYTAB} -K ${K5START_CCREFRESH} -u ${K5START_PRINCIPAL} -k ${K5START_CCFILE}
			echo "done."
		fi
		echo -n "Starting 6PO: "
		start-stop-daemon --start --quiet -b -m --pidfile ${SISPOPID} --exec ${SISPOPATH}
		echo "done."
		;;
	stop)
		echo -n "Stopping 6PO: "
		start-stop-daemon --stop --quiet --pidfile ${SISPOPID}
		sleep 1
		if [ -f ${SISPOPID} ] && ! ps h `cat ${SISPOPID}` > /dev/null
		then
			rm -f ${SISPOPID}
		fi
		echo "done."
		if [ "$K5START_START" = "yes" ]
		then
			echo -n "Stopping K5START: "
			start-stop-daemon --stop --quiet --pidfile ${K5START_PIDFILE}
			sleep 1
			if [ -f ${K5START_PIDFILE} ] && ! ps h `cat ${K5START_PIDFILE}` > /dev/null
			then
				rm -f ${K5START_PIDFILE}
			fi
			echo "done."
		fi

		;;
	restart)
		$0 stop
		sleep 1
		$0 start
		;;
	status)
		if [ "$K5START_START" = "yes" ]
		then
			if [ -f ${K5START_PIDFILE} ] && ps h `cat ${K5START_PIDFILE}` > /dev/null
			then
				echo "K5START is running (pid `cat ${K5START_PIDFILE}`)."
			else 
				echo "K5START is not running."
			fi
		fi
		if [ -f ${SISPOPID} ] && ps h `cat ${SISPOPID}` > /dev/null
		then
			echo "6PO is running (pid `cat ${SISPOPID}`)."
		else 
			echo "6PO is not running."
		fi
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 1
		;;
esac

exit 0
