#!/usr/bin/perl
#
#  ********************************* ENGLISH *********************************
#  
#  --- Copyright notice :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Statement of copying permission
#  
#  This file is part of 6PO.
#  
#  6PO is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  6PO is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with 6PO; if not, write to the Free Software
#  Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#  
#  *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
# 
#  --- Notice de Copyright :
#  
#  Copyright 2015 DOSI AMU (Frédéric Bloise)
#  
#  
#  --- Déclaration de permission de copie
# 
#  Ce fichier fait partie de 6PO.
#  
#  6PO est un logiciel libre : vous pouvez le redistribuer ou le modifier
#  selon les termes de la Licence Publique Générale GNU telle qu'elle est
#  publiée par la Free Software Foundation ; soit la version 3 de la Licence,
#  soit (à votre choix) une quelconque version ultérieure.
#  
#  6PO est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
#  GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
#  d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
#  pour plus de détails.
#  
#  Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
#  6PO ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
#

use strict;
use warnings;
use IO::Socket;
use Encode qw( decode_utf8 encode_utf8 );
use YAML::Tiny;
use Net::CUPS;
use File::Path qw(make_path remove_tree);
use Net::LDAP;
use DBI;
use Getopt::Long;
use MIME::Base64;
use POSIX qw( strftime );
use File::Pid;

$SIG{CHLD} = 'IGNORE';

my $yaml = YAML::Tiny->read('6po.yml') or die "Impossible d'ouvrir le fichier 6po.yml" ;
my $SERVERS = $yaml->[0]->{servers};
my $DB = $yaml->[0]->{db};
my $LDAP = $yaml->[0]->{ldap};
my $APACHE = $yaml->[0]->{apache};
my $SISPOWEB = $yaml->[0]->{sispoweb};
my $SISPODAV = $yaml->[0]->{sispodav};
my $PATTERNS = $yaml->[0]->{patterns};

sub decode  { 
	my ($rep,$k0,$k1,$k2,$k3) = @_;
	my @v = map hex, $rep =~ /......../g;
	my $r_utf8="";
	for (my $i=0;$i<$#v;$i+=2) {
		my $v0=$v[$i];
		my $v1=$v[$i+1];
		my $sum = 0; my $n = 0;
		$sum = 0x9e3779b9 << 5 ;
		while ($n<32) {
			$v1 -= (($v0<<4)+$k2) ^ ($v0+$sum) ^ ((0x07FFFFFF & ($v0>>5))+$k3) ;
			$v1 &= 0xFFFFFFFF;
			$v0 -= (($v1<<4)+$k0) ^ ($v1+$sum) ^ ((0x07FFFFFF & ($v1>>5))+$k1) ;
			$v0 &= 0xFFFFFFFF;
			$sum -= 0x9e3779b9 ;
			$n++;
		}
		$r_utf8.=sprintf("%08x%08x",$v0,$v1);
	}
	my @bytes = map hex, $r_utf8 =~ /../g;
	if ($#bytes>0) {while ( ! $bytes[$#bytes] ) { pop (@bytes); }}
	my $b_utf8 = join '', map chr, @bytes;
	decode_utf8($b_utf8);
	return $b_utf8;
}

sub normalizeLocation{
	my ($location)=@_;
	$location=~s/é/e/g;
	$location=~s/ê/e/g;
	$location=~s/à/a/g;
	$location=~s/ù/u/g;
	return $location;
}

sub locationToDavPath{
	my ($site,$location,$patterns)=@_;
	$location=normalizeLocation($location);
	my $davpath="";
	my $separator=" ";
	if (defined $patterns->{separator}) { $separator=$patterns->{separator}; }
	if (defined $patterns->{list}) {
		foreach my $pattern (@{$patterns->{list}}) {
			my $head=$location;
			$head=~s/^($pattern).*/$1/i;
			$location=~s/^$pattern//i;
			$location=~s/^$separator//;
			if (! ($location eq $head)) {
				$davpath.=$head."/";
			}
		}
	} else {
		$location=~s/$separator/\//g;
	}
	if (!($davpath eq "")) { chop($davpath); }
	else {$davpath=$location; }
	if (!($site eq "_default_")) { $davpath=$site."/".$davpath; }
	return $davpath;
}

sub addPrinter {
	my ($printer,$davpath,$duplex,$color,$server,$type)=@_;
	my $printer_cups=$printer;
	$printer_cups=~s/ /_/g;
	if ($SISPODAV->{active}) {
		$davpath=$SISPODAV->{dir}.$davpath;
		make_path($davpath,{owner=>'www-data', group=>'www-data',mode => 0555});
		$davpath.="/$printer_cups";	
		if (! $color && ! $duplex) {
			make_path($davpath,{owner=>'www-data', group=>'www-data',mode => 0755});
		} 
		else {
			make_path($davpath,{owner=>'www-data', group=>'www-data',mode => 0555});
			if ($color) {
				if ($duplex) {
					make_path($davpath."/couleur",{owner=>'www-data', group=>'www-data',mode => 0555});	
					make_path($davpath."/couleur/recto",{owner=>'www-data', group=>'www-data',mode => 0755});
					make_path($davpath."/couleur/recto-verso",{owner=>'www-data', group=>'www-data',mode => 0755});
					make_path($davpath."/couleur/recto-verso-bord-court",{owner=>'www-data', group=>'www-data',mode => 0755});
					make_path($davpath."/noir_et_blanc",{owner=>'www-data', group=>'www-data',mode => 0555});	
					make_path($davpath."/noir_et_blanc/recto",{owner=>'www-data', group=>'www-data',mode => 0755});
					make_path($davpath."/noir_et_blanc/recto-verso",{owner=>'www-data', group=>'www-data',mode => 0755});
					make_path($davpath."/noir_et_blanc/recto-verso-bord-court",{owner=>'www-data', group=>'www-data',mode => 0755});
				}
				else {
					make_path($davpath."/couleur",{owner=>'www-data', group=>'www-data',mode => 0755});	
					make_path($davpath."/noir_et_blanc",{owner=>'www-data', group=>'www-data',mode => 0755});	
				}
					
			} 
			else {
				make_path($davpath."/recto",{owner=>'www-data', group=>'www-data',mode => 0755});
				make_path($davpath."/recto-verso",{owner=>'www-data', group=>'www-data',mode => 0755});
				make_path($davpath."/recto-verso-bord-court",{owner=>'www-data', group=>'www-data',mode => 0755});
			}
		}
	}

	if ($type eq "windows") {
		my $cups = Net::CUPS->new();
		my $printer_uri=$printer;
		$printer_uri=~s/ /%20/g;
		$cups->setServer("127.0.0.1");
		$cups->addDestination($server."_".$printer_cups, $server, "", "6PO_PDF.PPD", "lpd://".$server."/$printer_uri");
	}
}

sub removePrinter {
	my ($printer,$davpath,$server,$type)=@_;
	my $printer_cups=$printer;
	$davpath=$SISPODAV->{dir}.$davpath."/".$printer;
	remove_tree($davpath);
	if ($type eq "windows") {
		$printer_cups=~s/ /_/g;
		my $cups = Net::CUPS->new();
		$cups->deleteDestination($server."_".$printer_cups);
	}
}
	
sub addApacheRequire {
	my ($printer,$allowusers,$davpath,$type)=@_;
	my $lh = Net::LDAP->new($LDAP->{uri},timeout=>5) or die("LDAP: impossible d'etablir la connexion avec ".$LDAP->{uri});
	my $mesg;
	if ($LDAP->{anonymous}) {
		$mesg = $lh->bind() or die("LDAP: bind anonymous impossible");
	}
	else {
		$mesg = $lh->bind($LDAP->{bind_user}, password => $LDAP->{bind_password}) or die("LDAP: bind impossible");
	}
	$davpath=$SISPODAV->{dir}.$davpath."/".$printer;
	my $section="<Directory \"$davpath\">\n";
	my @tabusers=split(",",$allowusers);
	foreach my $user (@tabusers) { 
		if ($user eq "Tout le monde") {
			return "";
		}
		if ($user eq "Utilisateurs du domaine") {
			$section.="\tRequire ldap-group CN=Utilisateurs du domaine,OU=Users,".$LDAP->{base_dn}."\n";
			$section.="\tRequire ldap-attribute primaryGroupID=513\n";
		}
		else {
			if ($type eq "ad") {
				$mesg = $lh->search(base=>$LDAP->{group_base_dn},scope=>"sub",filter =>"(&(cn=$user)(objectClass=group))",attrs=>[("primaryGroupToken")]);
				$mesg->code && die("ldap search : ".$mesg->error);
				if ($mesg->count()!=0) {
					foreach my $entry ($mesg->entries) {
						if ($LDAP->{nested_group}) {
							$section.="\tRequire ldap-filter memberof:1.2.840.113556.1.4.1941:=".$entry->{'asn'}->{'objectName'}."\n";
						} else {
							$section.="\tRequire ldap-group ".$entry->{'asn'}->{'objectName'}."\n";
						}
						$section.="\tRequire ldap-attribute primaryGroupID=".$entry->{'asn'}->{'attributes'}->[0]->{vals}->[0]."\n";
					}
				}	
				else {
					$section.="\tRequire ldap-user $user\n"; 
				}
			} 
			else {
				$mesg = $lh->search(base=>$LDAP->{group_base_dn},scope=>"sub",filter =>"(&(cn=$user)(objectClass=posixGroup))");
				$mesg->code && die("ldap search : ".$mesg->error);
				if ($mesg->count()!=0) {
					foreach my $entry ($mesg->entries) {
    	    					$section.="\tRequire ldap-group ".$entry->{'asn'}->{'objectName'}."\n"; 
					}
				}	
				else {
					$section.="\tRequire ldap-user $user\n"; 
				}
			}
		}
	}
	#$section.="\tSatisfy any\n";
	$section.="</Directory>\n";
	$lh->unbind();
	$lh->disconnect();
	return $section;
}

sub createPHPEntry {
	my ($printer,$location,$info,$allowusers,$denyusers,$duplex,$color,$a3)=@_;
	$printer=~s/ /_/g;
	my @tabusers=split(",",$allowusers);
	$allowusers=~s/([^,]*),/'$1',/g;
	chop($allowusers);
	$denyusers=~s/([^,]*),/'$1',/g;
	chop($denyusers);
	my $entry="\$printers['$printer']=array(\n";
	$entry.="  'location'=>'$location',\n";
	$entry.="  'info'=>'$info',\n";
	$entry.="  'allowusers'=>array($allowusers),\n";
	$entry.="  'denyusers'=>array($denyusers),\n";
	$entry.="  'duplex'=>$duplex,\n";
	$entry.="  'color'=>$color,\n";
	$entry.="  'a3'=>$a3,\n";
	$entry.=");\n";
	return $entry;
}
	
sub userInList {
	my ($user,$userlist)=@_;
	return $userlist=~/^$user,/i || $userlist=~/,$user,/i;
}	

my $pidfile = File::Pid->new( { file => '/var/run/collector.pid', pid => $$ } );
if (my $num = $pidfile->running ) {
	die "Le process collector.pl est déjà en cours d'exécution : $num\n";
} 
$pidfile->write;

my $date = strftime "[%d/%m/%Y:%H:%M:%S]", localtime;
print "$date Exécution de collector @ARGV\n";

my $dbh;
$dbh=DBI->connect("dbi:SQLite:dbname=6po.db") or die "Echec de connexion à la base sqlite\n";

my $sql_query;
my $sth;
my $full="";
my $cups_reset="";
my $show_location ="";
my $full_safe ="";
my $global_changes=0;
my $apache_conf_dir;
if (-d "/etc/apache2/conf.d/") { $apache_conf_dir="/etc/apache2/conf.d/"; }
elsif (-d "/etc/apache2/conf-enabled/") { $apache_conf_dir="/etc/apache2/conf-enabled/"; }
else { die "Impossible de trouver le répertoire de configuration d'Apache"; }

GetOptions ('full' => \$full,'cups-reset' => \$cups_reset,'show-location' => \$show_location,'full-safe' => \$full_safe);
if ($full) {
	$sql_query = "DELETE FROM printers";
	$sth=$dbh->prepare($sql_query);
	$sth->execute();
	$sth->finish();
	remove_tree($SISPODAV->{dir},{keep_root => 1});
	unlink glob ($apache_conf_dir."printers*");
	unlink glob ($SISPOWEB->{dir}."include/printers*");
	if ($cups_reset) {
		my $deleted=0;
		my $cups = Net::CUPS->new();
		$cups->setServer("127.0.0.1");
		my @printers = $cups->getDestinations();
		foreach my $printer (@printers) {
			$cups->deleteDestination($printer->getName());
			$deleted++;
		}
		print "$deleted imprimantes supprimées dans CUPS local.\n";
	}
}

$sql_query = "DELETE FROM changes";
$sth=$dbh->prepare($sql_query);
$sth->execute();
$sth->finish();

foreach my $server (keys(%{$SERVERS})) {

	my $SERVER=$SERVERS->{$server};

	print "Traitement du serveur $SERVER->{ip}\n";

	socket(SOCKET,PF_INET,SOCK_STREAM,(getprotobyname('tcp'))[2]) 
		or die "Impossible de créer le socket : $!\n";
	setsockopt(SOCKET, SOL_SOCKET, SO_RCVTIMEO, pack('l!l!', 60, 0) );
	connect( SOCKET, pack_sockaddr_in($SERVER->{sisport}, inet_aton($SERVER->{ip})))
		or ( print("Impossible de se connecter sur $SERVER->{ip} $SERVER->{sisport} : $! \n") and next ); 
	
	my $line;
	my $TEA = $yaml->[0]->{tea};
	my $site="_default_";
	if (defined $SERVER->{site}) {
		$site=$SERVER->{site};
	}
	
	if ($full_safe) {
		$sql_query = "DELETE FROM printers WHERE site='$site'";
		$sth=$dbh->prepare($sql_query);
		$sth->execute();
		$sth->finish();
		remove_tree($SISPODAV->{dir}.$site,);
		unlink ($apache_conf_dir."printers_".$SERVER->{ip}.".conf");
		unlink ($SISPOWEB->{dir}."include/printers_".$SERVER->{ip}.".php");
	}	

	my $i=0;
	my $encoded_lines = <SOCKET>;
	if ($encoded_lines && decode($encoded_lines,$TEA->{k0},$TEA->{k1},$TEA->{k2},$TEA->{k3}) eq "START") {
		my @lt = localtime(time);
		my $now=strftime("%Y-%m-%d %H:%M:%S",@lt);
		my $ignored=0;
		while ($encoded_lines = <SOCKET>) {
			$line=decode($encoded_lines,$TEA->{k0},$TEA->{k1},$TEA->{k2},$TEA->{k3});
			my ($printer,$location,$info,$allowusers,$denyusers,$duplex,$color,$color_option,$color_c,$color_nb,$a3)=split(":",$line);
			$allowusers=~s/, /,/g;
			$denyusers=~s/, /,/g;
			if ( (defined $SERVER->{deny} && userInList($SERVER->{deny},$denyusers)) || (defined $SERVER->{allow} && ! userInList($SERVER->{allow},$allowusers)) ) {
				$ignored++;
				next;
			}
			my $davpath;
			if (defined $SERVER->{patterns}) { $davpath=locationToDavPath($site,$location,$SERVER->{patterns}); }
			else { $davpath=locationToDavPath($site,$location,$PATTERNS); }
			if ($show_location) {
				print "$location => $davpath\n";
			}
			$sql_query = "UPDATE OR IGNORE printers SET site='$site',location='".decode_utf8($location)."',info='$info',allowusers='$allowusers',denyusers='$denyusers',duplex='$duplex',color='$color',color_option='$color_option',color_c='$color_c',color_nb='$color_nb',a3='$a3',lastseen='$now' WHERE printer='$printer' AND site='$site'";
			$sth=$dbh->prepare($sql_query);
			$sth->execute();
			$sth->finish();
			$sql_query = "INSERT OR IGNORE INTO printers(printer,site,location,info,allowusers,denyusers,davpath,duplex,color,color_option,color_c,color_nb,a3,insertedat,lastseen) VALUES ('$printer','$site','".decode_utf8($location)."','$info','$allowusers','$denyusers','$davpath','$duplex','$color','$color_option','$color_c','$color_nb','$a3','$now','$now')";
			$sth=$dbh->prepare($sql_query);
			$sth->execute();
			$sth->finish();
		}
		my $added=0;
		my @deleted;
		my $apacheRequireText="";
		my $davpath;
		$sql_query = "SELECT count(printer) FROM changes WHERE old_allowusers<>new_allowusers AND site='$site'";
		$sth=$dbh->prepare($sql_query);
		$sth->execute();
		my ($changes_allowusers)=$sth->fetchrow_array();
		$sth->finish();
		$sql_query = "SELECT count(printer) FROM changes WHERE old_denyusers<>new_denyusers AND site='$site'";
		$sth=$dbh->prepare($sql_query);
		$sth->execute();
		my ($changes_denyusers)=$sth->fetchrow_array();
		$sth->finish();
		$sql_query = "SELECT printer,location,info,allowusers,davpath,duplex,color,a3,insertedat,lastseen FROM printers WHERE site='$site'";
		$sth=$dbh->prepare($sql_query);
		$sth->execute();
		while (my ($printer,$location,$info,$allowusers,$davpath,$duplex,$color,$a3,$insertedat,$lastseen)=$sth->fetchrow_array()) {
			if ($insertedat eq $now) {
				addPrinter($printer,$davpath,$duplex,$color,$SERVER->{ip},$SERVER->{type});
				$apacheRequireText.=addApacheRequire($printer,$allowusers,$davpath,$LDAP->{type});
				$added++;
			}
			elsif ($lastseen eq $now) {
				if ($changes_allowusers) {
					$apacheRequireText.=addApacheRequire($printer,$allowusers,$davpath,$LDAP->{type});
				}
			}
			else {
				removePrinter($printer,$davpath,$SERVER->{ip},$SERVER->{type});
				push @deleted,$printer;
			}
		}
		$sth->finish();
		if ( ($added>0) || $changes_allowusers) {
			$global_changes++;
			open(my $fh_ht, ">", $apache_conf_dir."printers_".$SERVER->{ip}.".conf");
			print $fh_ht $apacheRequireText;
			close($fh_ht);
		}	
		if ( (@deleted>0) ) {
			$sql_query = "DELETE FROM printers WHERE printer in ('".join("','", @deleted)."') AND site='$site'";
			$sth=$dbh->prepare($sql_query);
			$sth->execute();
			$sth->finish();
		}
		if ( ($added>0) ||(@deleted>0) || $changes_allowusers || $changes_denyusers) {
			my $php="<?php\n\n";
			$php.="\$printers=Array();\n\n";
			$sql_query = "SELECT printer,location,info,allowusers,denyusers,duplex,color,a3 FROM printers WHERE site='$site' ORDER BY location,printer";
			$sth=$dbh->prepare($sql_query);
			$sth->execute();
			while (my ($printer,$location,$info,$allowusers,$denyusers,$duplex,$color,$a3) =$sth->fetchrow_array()) {
				$php.=createPHPEntry($printer,$location,$info,$allowusers,$denyusers,$duplex,$color,$a3);
			}
			open(my $fh_php, ">", $SISPOWEB->{dir}."include/printers_".$SERVER->{ip}.".php");
			print $fh_php $php;
			close ($fh_php);
			$sth->finish();
		}
		print "ajouts : $added\n"; 
		printf("supressions : %d\n",scalar @deleted); 
		print "ignorées : $ignored\n"; 
		print "modifications allowusers : $changes_allowusers\n"; 
		print "modifications denyusers : $changes_denyusers\n"; 
	} 
	else {
		print "Mauvais chiffrement ou temps de connexion dépassé !\n";
	}
	close SOCKET or die "close: $!";
}

if ($global_changes) {
	system($APACHE->{reload})
}

$pidfile->remove;
